from django.core.files.storage import FileSystemStorage
from django.shortcuts import render
from django.http import HttpResponse
from . import pdfreader
import pathlib
import os

def index(request):
    return render(request, "index.html")


def getResults(request):
    if request.method == 'POST':
        file_path = request.FILES['filepath']
        name, ext = os.path.splitext(file_path.name)
        new_name = "name" + '.pdf'
        fs = FileSystemStorage()
        name = fs.save(new_name, file_path)
        file_url = "."+fs.url(name)
        get_pdf_contents = pdfreader.getPdfContents(file_url)
        get_paragraph_blocks = pdfreader.getParagraphs(file_url)
        get_words = pdfreader.getWords(file_url)
        get_html = pdfreader.getHtml(file_url)
    file_to_rem = pathlib.Path(file_url)
    file_to_rem.unlink()
    return render(request, "cvanalytics.html",
                  {'get_pdf_contents': get_pdf_contents, 
                  'file_path': file_path,
                  'get_paragraph_blocks': get_paragraph_blocks,
                  'get_words':get_words,
                  'get_html':get_html
    })

