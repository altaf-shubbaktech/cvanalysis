
from tkinter import font
import fitz


#extracting text from pdf
def getPdfContents(fname):
    text = []
    doc = fitz.open(fname)
    for pages in doc:
        text.append(pages.getText('dict'))
    return text


#get paragraph blocks
def getParagraphs(fname):
    blocks = []
    doc = fitz.open(fname)
    for pages in doc:
        blocks.append(pages.getText('blocks'))
    return blocks

#get total words
def getWords(fname):
    words = []
    noofwords = 0
    doc = fitz.open(fname)
    for pages in doc:
        words.append(pages.getText('words'))
        noofwords = noofwords+len(pages.getText('words'))
    words.append(noofwords)
    return words


#get html code
def getHtml(fname):
    html_lst = []
    doc = fitz.open(fname)
    for pages in doc:
        html_lst.append(pages.getText('html'))
    return html_lst


#get font list
# def getFontList(fname):
#     text = []
#     font_list = []
#     doc = fitz.open(fname)
#     for pages in doc:
#         text.append(pages.getText('dict'))
#     for t in text:
#         for b in t['blocks']:
#             for l in b['lines']:
#                 for s in l['spans']:
#                     font_list.append(s['font'])
#     font_list = list(dict.fromkeys(font_list))
#     font_list.sort()
#     return font_list